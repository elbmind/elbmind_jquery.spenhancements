/**
 * SPEnhancements - Enhancing SharePoint (2010 / 2013) Forms using jQuery
 * @version 0.1.12 beta
 * @name SPEnhancements
 * @description Enhance your SharePoint Forms using jQuery
 * @type jQuery
 * @category Plugins/SPServices
 * @requires jQuery v1.8 or greater
 * @author David Kahnt, André Mommert | mail@elbmind.com
 * @copyright SPEnhancements v0.1.12 beta | (c) 2019 elbmind GmbH | elbmind.com
 * @license MIT
 */

/* eslint-disable */

(function ($) {

    /**
     * Versionstring
     * @type {String}
     */

    var selectorOutput = [];
    var speDefaults = {
        "VERSION": "0.1.12 beta",
        "fileName": window.location.pathname.match('/([^/]*?)(\\.[^\\./]*)?$')[1].toLowerCase(),
        "decimalDelimiter": "."
    };
    var speOverwrites = {};
    // var sharepointVersion = (typeof SP !== 'undefined') ? parseInt(SP.ClientSchemaVersions.currentVersion) : 12;
    // var sharepointVersion = 14;

    /**
     * Return the current version of SPEnhancements as a string
     *
     * @method SPEnhancements
     * @param {Object} options
     */

    $.fn.SPEnhancements = function (options) {
        // global settings
        // TODO: implement setFormFieldSelectors function related to SPSetFormFieldSelectors
        speOverwrites = $.extend(speDefaults, speOverwrites, options);
        return speOverwrites;
    };

    /**
     * Return the current version of SPEnhancements as a string
     *
     * @method Version
     * @return {String} Versionstring
     */

    $.fn.SPEnhancements.Version = $().SPEnhancements().VERSION;
    $.fn.SPEnhancements.fileName = $().SPEnhancements().fileName;
    $.fn.SPEnhancements.decimalDelimiter = $().SPEnhancements().decimalDelimiter;
    $.fn.SPEnhancements.selectorOutput = selectorOutput;
    // $.fn.SPEnhancements.sharepointVersion = sharepointVersion;

    /**
     * This method adds classes to the table rows of the "main" form table, depending on the internal field descriptions
     *
     * @method SPSetFormFieldSelectors
     * @param {Object} options
     */

    $.fn.SPSetFormFieldSelectors = function (options) {
        var opt = $.extend({
            "relatedData": false,
            "returnInfo": false,
            "removeSelectors": false,
            "sharepointVersion": 14
        }, options);
        var formTableHook,
            m,
            replaceString = /^\"|\"$/g;
        // check SharePoint version
        if (opt.sharepointVersion <= 14) {
            formTableHook = 'div[id^=WebPartWPQ] table.ms-formtable:first';
        } else {
            formTableHook = '#tbAllFields';
        }
        // append spe-class for easy form table selection
        $(formTableHook).addClass('speForm');
        if (opt.relatedData) {
            // append area for related form data
            $('.speForm').closest('div[id^=WebPartWPQ]').append('<span class="speRelatedData">&nbsp;</span>');
        }
        // check if contenttype filed is in dom
        var checkContentTypeField = $('.speForm .ms-formbody select[id*="ContentTypeChoice"]', 'div[id^=WebPartWPQ]').attr('title');
        if (!!checkContentTypeField) {
            // add item to json
            selectorOutput.push({
                "FieldName": checkContentTypeField,
                "FieldInternalName": "ContentType",
                "FieldType": "SPFieldContentType",
                "FormRow": 0
            });
        }
        m = (selectorOutput.length === 1) ? 1 : 0;
        $('.speForm .ms-formbody').contents().filter(function () {
            // walking trough dom for all 'ms-formbody' classes, finding all html comments
            return this.nodeType == 8;
        }).each(function (index, element) {
            var n = element.nodeValue.match(/".*"/g);
            selectorOutput.push({
                "FieldName": n[0].replace(replaceString, ''),
                "FieldInternalName": n[1].replace(replaceString, ''),
                "FieldType": n[2].replace(replaceString, ''),
                "FormRow": m++
            });
        });
        var ele = $('.speForm > tbody', 'div[id^=WebPartWPQ]');
        for (var i = 0; i < m; i++) {
            ele.children('tr:eq(' + (selectorOutput[i].FormRow) + ')').addClass(selectorOutput[i].FieldInternalName + ' ' + selectorOutput[i].FieldType + ' SPFieldRow');
        }
        if (opt.returnInfo) {
            return {
                "contentTypeFieldAvailable": checkContentTypeField,
                "numberOfModifiedRows": selectorOutput.length
            };
        } else {
            return true;
        }
    };

    /**
     * just keeping backward compatability
     */

    $.fn.SPLookupIdVal = function (value) {
        return $(this).SPFieldVal(value);
    };

    /**
     * Method to get or set the field content
     * at the moment, this is not working for all field types
     *
     * @method SPFieldVal
     * @param {Object} value
     */

    $.fn.SPFieldVal = function (value) {
        // define vars
        var inputFieldElement,
            inputFieldElementDate,
            inputFieldElementHour,
            inputFieldElementIdAltered,
            inputFieldElementIdPlain,
            inputFieldElementMins,
            inputFieldLookup,
            selectFieldLookup;
        // check input field type
        if (this.hasClass('SPFieldText')) {
            // find input dom element
            inputFieldElement = this.find('.ms-formbody input:first');
            // check if value should be set
            if (typeof value !== "undefined") {
                // set contents of 'value' into input field
                inputFieldElement.val(value);
                // update locked field state
                if ($(this).hasClass('SPFieldLocked')) {
                    $(this).SPLockField();
                }
                // return selectors to keep jquery chaining
                return this;
                // check if input field is not empty
            } else if (inputFieldElement.length > 0) {
                // return input field value
                return inputFieldElement.val();
            } else {
                // if there is nothing to set or get than return false
                return false;
            }
        } else if (this.hasClass('SPFieldCurrency') || this.hasClass('SPFieldNumber')) {
            // find input dom element
            inputFieldElement = this.find('.ms-formbody input:first');
            // check if value should be set
            if (typeof value !== "undefined") {
                // set contents of 'value' into input field
                inputFieldElement.val(value);
                // update locked field state
                if ($(this).hasClass('SPFieldLocked')) {
                    $(this).SPLockField();
                }
                // return selectors to keep jquery chaining
                return this;
                // check if input field is not empty
            } else if (inputFieldElement.length > 0) {
                // format the number according to the global decimal delimiter setting
                var returnValue = inputFieldElement.val();
                var decimal = speOverwrites.decimalDelimiter;
                var regex = new RegExp('[^0-9-' + decimal + ']', ['g']);
                var unformatted = parseFloat(('' + returnValue).replace(/\((.*)\)/, '-$1').replace(regex, '').replace(decimal, '.'));
                // return parsed number input field value
                return unformatted;
            } else {
                // if there is nothing to set or get than return false
                return false;
            }
        } else if (this.hasClass('SPFieldUser')) {
            // find input dom element for user field
            inputFieldElement = this.find('.ms-formbody .ms-inputuserfield');
            // transform field id
            inputFieldElementIdPlain = inputFieldElement.attr('id').replace('_upLevelDiv', '');
            inputFieldElementIdAltered = inputFieldElementIdPlain.replace('ctl00_m_g', 'ctl00$m$g').replace('_UserField', '$UserField').replace(/\_ctl/g, '$ctl');
            // check if value should be set
            if (typeof value !== "undefined") {
                // set contents of 'value' into input field
                // inputFieldElement.val(value);
                if (value === false) {
                    value = '';
                }
                WebForm_DoCallback(inputFieldElementIdAltered, value, EntityEditorHandleCheckNameResult, inputFieldElementIdPlain, EntityEditorHandleCheckNameError, true);
                // update locked field state
                if ($(this).hasClass('SPFieldLocked')) {
                    $(this).SPLockField();
                }
                // return selectors to keep jquery chaining
                return this;
                // check if input field is not empty
            } else if ($.trim(inputFieldElement.text()).length > 0) {
                // return input field contents in form of dom object for each person
                return inputFieldElement.children();
            } else {
                // if there is nothing to set or get than return false
                return false;
            }
        } else if (this.hasClass('SPFieldBoolean')) {
            // find input dom element for user field
            inputFieldElement = this.find('.ms-formbody input[type="checkbox"]:first');
            // check if value should be set
            if (typeof value !== "undefined" && (value === true || value === false)) {
                // set contents of 'value' into input field
                // inputFieldElement.val(value);
                inputFieldElement.prop('checked', value);
                // update locked field state
                if ($(this).hasClass('SPFieldLocked')) {
                    $(this).SPLockField();
                }
                // return selectors to keep jquery chaining
                return this;
                // check if input field is not empty
            } else if (inputFieldElement) {
                // return input field contents in form of dom object for each person
                return inputFieldElement.is(':checked');
            } else {
                // if there is nothing to set or get than return false
                return false;
            }
        } else if (this.hasClass('SPFieldDateTime')) {
            // find input dom elements
            inputFieldElementDate = this.find('.ms-formbody .ms-dtinput input');
            inputFieldElementHour = this.find('.ms-formbody .ms-dttimeinput select:first');
            inputFieldElementMins = this.find('.ms-formbody .ms-dttimeinput select:last');
            // check if value should be set
            if (typeof value !== "undefined" && $.isArray(value)) {
                // set contents of 'value' into date input field
                inputFieldElementDate.val(value[0]);
                // check if time values exist
                if (value.length === 3) {
                    inputFieldElementHour.val(value[1]);
                    inputFieldElementMins.val(value[2]);
                }
                // update locked field state
                if ($(this).hasClass('SPFieldLocked')) {
                    $(this).SPLockField();
                }
                // return selectors to keep jquery chaining
                return this;
                // check if input field is not empty
            } else if (value === '' || value === false) {
                // wipeout the fields
                inputFieldElementDate.val('');
                inputFieldElementHour.val('');
                inputFieldElementMins.val('');
            } else if (inputFieldElementDate.length > 0) {
                // return input field value
                return [inputFieldElementDate.val(), inputFieldElementHour.val(), inputFieldElementMins.val()];
            } else {
                // if there is nothing to set or get than return false
                return false;
            }
        } else if (this.hasClass('SPFieldLookup')) {
            // find input or select dom element
            inputFieldLookup = this.find('input.ms-lookuptypeintextbox:first');
            selectFieldLookup = this.find('select[id*="Lookup"]:first');
            if (value === 0) {
                // check input value to set
                inputFieldLookup.val('');
                $('#' + inputFieldLookup.attr('optHid')).val(parseInt(value, 10));
                selectFieldLookup.val(0);
            } else if (typeof value !== "undefined") {
                if (inputFieldLookup.length > 0 && value >= 0) {
                    var choiceArray = inputFieldLookup.attr('choices').match(/(?:[^\|]|\|\|)+/g);
                    for (var index = 1; index < choiceArray.length; index += 2) {
                        if (choiceArray[index] === value) {
                            inputFieldLookup.val(choiceArray[index - 1]);
                            return $('#' + inputFieldLookup.attr('optHid')).val(parseInt(value, 10));
                        }
                    }
                    return false;
                } else if (selectFieldLookup.length > 0 && value >= 0) {
                    return selectFieldLookup.val(parseInt(value, 10));
                } else {
                    return false;
                }
            } else {
                if (inputFieldLookup.length > 0) {
                    return parseInt($('#' + inputFieldLookup.attr('optHid')).val() ? $('#' + inputFieldLookup.attr('optHid')).val() : 0, 10);
                } else if (selectFieldLookup.length > 0) {
                    return parseInt(selectFieldLookup.children('option:selected').val(), 10);
                } else {
                    return '';
                }
            }
        } else {
            // return false if field type is not correct
            return false;
        }
        // return selectors to keep jquery chaining
        return this;
    };

    /**
     * TODO method to create a dropdown from a choice list based on text or number fields OR lookup colums greater than 20 items
     *
     * @method SPAddCustomDropdown
     * @param {Object} options
     */

    $.fn.SPAddCustomDropdown = function (options) {
        var opt = $.extend({
            "fieldTitle": this.find('.ms-standardheader').attr('title'),
            "fieldSelectorLabelOutput": false,
            "dropdownCoices": false,
            "positionOfDropdown": this,
            "positionIsAfter": false,
            "simpleDropdown": false,
            "simpleDropdownCallback": false,
            "selectedItem": false
        }, options);
        if ((opt.dropdownCoices && this.hasClass('SPFieldText') || this.hasClass('SPFieldNumber') || this.hasClass('SPFieldLookup')) || opt.simpleDropdown) {
            var fieldItem = this.find('input:first');
            if (fieldItem.length === 0) {
                return false;
            }
            var newElementId = this.attr('class').replace(/ /gi, '');
            var strFieldItemVal = '',
                strInputChoices = '',
                strSelectChoices = '';
            var bolInitMatch = false;
            var dropdownHtml;
            $('.' + newElementId).remove();
            $.each(opt.dropdownCoices, function (index, element) {
                strInputChoices += element.label + '|' + element.key + '|';
                strSelectChoices += '<option ' + (opt.selectedItem === element.key || opt.selectedItem === element.label ? 'selected="selected"' : '') + ' value="' + element.key + '" title="' + element.label + '">' + element.label + '</option>';
                if (fieldItem.val() === element.key) {
                    strFieldItemVal = element.label;
                    bolInitMatch = true;
                    if (opt.fieldSelectorLabelOutput) {
                        $(opt.fieldSelectorLabelOutput).val(element.label);
                    }
                }
            });
            if (!bolInitMatch) {
                fieldItem.val('');
                if (opt.fieldSelectorLabelOutput) {
                    $(opt.fieldSelectorLabelOutput).val('');
                }
            }
            strInputChoices = strInputChoices.replace(/\|$/, '');
            if (opt.simpleDropdown) {
                dropdownHtml = '<tr class="' + newElementId + ' SPFieldLookup SPFieldRow"><td nowrap="true" valign="top" width="190px" class="ms-formlabel"><h3 class="ms-standardheader"><nobr>' + opt.fieldTitle + '</nobr></h3></td><td valign="top" class="ms-formbody"><span dir="none"><span style="vertical-align:middle"><select name="_Select" tabIndex="-1" class="ms-lookuptypeindropdown" id="_Select" style="position: relative;">' + strSelectChoices + '</select></span><br/></span></td></tr>';
            } else {
                dropdownHtml = '<tr class="' + newElementId + ' SPFieldLookup SPFieldRow"><td nowrap="true" valign="top" width="190px" class="ms-formlabel"><h3 class="ms-standardheader"><nobr>' + opt.fieldTitle + '</nobr></h3></td><td valign="top" class="ms-formbody"><span dir="none"><span style="vertical-align:middle"><input name="' + newElementId + '" type="text" value="' + strFieldItemVal + '" id="' + newElementId + '" class="ms-lookuptypeintextbox" onfocusout="CoreInvoke(\'HandleLoseFocus\')" opt="_Select" title="' + opt.fieldTitle + '" optHid="SP' + newElementId + '_Hidden" onkeypress="CoreInvoke(\'HandleChar\')" onkeydown="CoreInvoke(\'HandleKey\')" match="" choices="' + strInputChoices + '" onchange="CoreInvoke(\'HandleChange\')" /><img alt="Nachschlagewerte anzeigen" onclick="CoreInvoke(\'ShowDropdown\',\'' + newElementId + '\');" src="/_layouts/images/dropdown.gif" style="border-width:0px;vertical-align:middle;" /><select name="_Select" tabIndex="-1" class="ms-lookuptypeindropdown" id="_Select" style="z-index: 2; position: absolute; display: none; top: 103px; left: 384px;" onkeydown="HandleOptKeyDown()" ondblclick="HandleOptDblClick()" onfocusout="OptLoseFocus(this)" ctrl="' + newElementId + '">' + strSelectChoices + '</select></span><br/></span></td></tr>';
            }
            opt.positionOfDropdown = opt.positionOfDropdown.hasClass('SPFieldRow') ? opt.positionOfDropdown : this;
            if (opt.positionIsAfter) {
                opt.positionOfDropdown.after(dropdownHtml);
            } else {
                opt.positionOfDropdown.before(dropdownHtml);
            }
            if (opt.simpleDropdown && !!opt.simpleDropdownCallback) {
                $('.' + newElementId).on('focusout change', opt.simpleDropdownCallback);
            } else {
                $('#aspnetForm > div:first').append('<input id="SP' + newElementId + '_Hidden" value="' + fieldItem.val() + '" type="hidden" name="SP' + newElementId + '_Hidden">');
                $('.' + newElementId).on('focusout change', function () {
                    fieldItem.val($('.' + newElementId).SPFieldVal());
                    if (opt.fieldSelectorLabelOutput) {
                        $(opt.fieldSelectorLabelOutput).val($('.' + newElementId + ' input').val());
                    }
                });
            }
        } else {
            return false;
        }
        // return selectors to keep jquery chaining
        return this;
    };

    /**
     * __method__ helps you to "lock" sharepoint fields to prevent change
     * @method SPLockField
     * @param [options={}] {Object} the object whose properties will be rendered in the method
     * @chainable
     */

    $.fn.SPLockField = function (options) {
        var opt = $.extend({
            "showHintText": true,
            "unlockField": false,
            "useLookupUrl": false,
            "useLookupEditMode": false
        }, options);
        this.each(function () {
            // walk trough each selector
            var displayText = '';
            var matchedFieldType = false;
            // wrap the content of ms-formbody with a span if form is DispForm
            if (speOverwrites.fileName === "dispform" && !$(this).find('.ms-formbody > span').hasClass('SPDisplayText')) {
                // wrap it!
                $(this).find('.ms-formbody').wrapInner('<span class="SPWrappedRowBody" />');
            }
            // check custom lockfield classes
            if (!$(this).find('.ms-formbody > span').hasClass('SPDisplayText') && !$(this).find('.ms-formbody > span').hasClass('SPHintText')) {
                // check if SPDisplayText and SPHintText span is already present
                // prepend displaytext placeholder
                $(this).find('.ms-formbody').prepend('<span class="SPDisplayText"></span>');
                $(this).find('.ms-formbody').contents().filter(function () {
                    return this.nodeType === 3;
                }).wrap('<span class="SPHintText"></span>');
            }
            // collect display text
            if ($(this).hasClass('SPFieldContentType') || $(this).hasClass('SPFieldLookup') || $(this).hasClass('SPFieldText') || $(this).hasClass('SPFieldNumber') || $(this).hasClass('SPFieldCurrency')) {
                // for contenttype field, single lookup fields, simple (single row) text fields and number fields do this
                displayText = $(this).find('.ms-formbody select').html() ? $(this).find('.ms-formbody select option:selected').text() : $(this).find('.ms-formbody input').val();
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldChoice')) {
                // detect dropdown or radio NOT multiple >> see SPFieldMultiChoice
                if ($(this).find('.ms-formbody .ms-RadioText').prop("tagName") === 'SELECT') {
                    // for option field
                    displayText = $(this).find('.ms-formbody .ms-RadioText option:selected').text();
                } else {
                    displayText = $(this).find('.ms-formbody .ms-RadioText :checked').next('label').text();
                }
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldMultiChoice')) {
                displayText = '';
                $(this).find('.ms-formbody .ms-RadioText :checked').each(function () {
                    displayText += $(this).next('label').text() + '<br />';
                });
                // remove last break
                displayText = displayText.slice(0, -6);
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldUser')) {
                // for people picker field
                displayText = $(this).find('.ms-formbody .ms-usereditor .ms-inputuserfield .ms-entity-resolved > span').text();
                displayText = displayText ? displayText.replace(/\d*;#/gi, "") : "";
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldDateTime')) {
                // for date time fields do this
                displayText = $(this).find('.ms-formbody .ms-dtinput input').val() + ' ' + $(this).find('.ms-formbody .ms-dttimeinput select:first option:selected').text() + $(this).find('.ms-formbody .ms-dttimeinput select:last option:selected').text();
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldBoolean')) {
                // for checkbox (yes/no) do this
                var labelText = $(this).find('.ms-formbody input:first').next('.SPFieldBooleanDisplayText').html();
                displayText = $(this).find('.ms-formbody input:first').is(':checked') ? '<img src="/_layouts/images/CbChecked.gif" border="0" alt="checked" style="vertical-align: text-bottom;">&nbsp;' : '<img src="/_layouts/images/CbUnChecked.gif" border="0" alt="unchecked" style="vertical-align: text-bottom;">&nbsp;';
                displayText += labelText ? labelText : '';
                matchedFieldType = true;
            } else if ($(this).hasClass('SPFieldNote')) {
                // for textarea do this
                displayText = $(this).find('.ms-formbody textarea:first').val().replace(/\n/g, '&nbsp;<br>');
                matchedFieldType = true;
            }
            if (opt.unlockField || options === false) {
                // if unlock is set remove SPDisplayText and remove SPHintText span and show original span
                $(this).find('.ms-formbody .SPDisplayText').remove();
                $(this).find('.ms-formbody .SPHintText').replaceWith(function () {
                    return $(this).contents();
                });
                $(this).find('.ms-formbody > span').show();
                $(this).removeClass('SPFieldLocked');
            } else if (matchedFieldType && $(this).hasClass('SPFieldRow')) {
                // "lock field" only if defined field types match and it is a SPEnhancements field row
                if (opt.useLookupUrl && $(this).hasClass('SPFieldLookup')) {
                    // TODO: add url to display text, if url is present - there is no url validation
                    // var urlParamVal = $(this).find('.ms-formbody select option:selected').val();
                    var urlParamVal = $(this).SPFieldVal();
                    // displayText = '<a href="javascript:SP.UI.ModalDialog.OpenPopUpPage(\'' + opt.useLookupUrl + 'Forms/DispForm.aspx?Source=' + escape(location.href) + '&IsDlg=1&ID=' + urlParamVal + '\', null, ' + ($(window).width() - 100) + ', ' + ($(window).height() - 100) + ');">' + displayText + '</a>';
                    displayText = '<a href="javascript:SP.UI.ModalDialog.OpenPopUpPage(\'' + opt.useLookupUrl + (opt.useLookupUrl.match('/Lists/') ? '' : 'Forms') + '/' + (opt.useLookupEditMode ? 'EditForm' : 'DispForm') + '.aspx?Source=' + escape(location.href) + '&IsDlg=1&ID=' + urlParamVal + '\', null, null, null);">' + displayText + '</a>';
                } else if (opt.useLookupUrl === false && $(this).hasClass('SPFieldLookup')) {
                    // if lookupurl is not defined 'just lock it' and display text
                    displayText = $(this).find('.ms-formbody a:first').length > 0 ? $(this).find('.ms-formbody a:first').text() : displayText;
                }
                // hide the formbody content
                $(this).find('.ms-formbody > span').hide();
                // show the display text
                $(this).find('.ms-formbody .SPDisplayText').show().html(displayText + '<br />');
                if (opt.showHintText) {
                    // show field description if show hint option is true
                    $(this).find('.ms-formbody .SPHintText').show();
                }
                if ($(this).find('.ms-formbody span.ms-formvalidation').length > 0) {
                    // show formvalidation warnings
                    $(this).find('.ms-formbody').append($(this).find('.ms-formbody span.ms-formvalidation').prop('outerHTML'));
                }
                // set class to easier identify locked fields
                $(this).addClass('SPFieldLocked');
            }
        });
        // return selectors to keep jquery chaining
        return this;
    };

    /**
     * With this function you got the ability to set different options using json
     *
     * @method SPUpdateLookupChoices
     * @param {Object} options dropdownCoices type: json is needed to build dropdown
     */

    $.fn.SPUpdateLookupChoices = function (options) {
        var opt = $.extend({
            "dropdownCoices": false,
            "selectedKey": false
        }, options);
        var strInputChoices = '',
            strSelectChoices = '';
        if (opt.dropdownCoices && typeof opt.dropdownCoices === "object" && this.hasClass('SPFieldLookup') && this.length === 1) {
            $.each(opt.dropdownCoices, function (index, element) {
                var tmpLabel = $('<span>' + String(element.label) + '</span>').text();
                var tmpKey = parseInt(element.key, 10);
                strInputChoices += tmpLabel + '|' + tmpKey + '|';
                strSelectChoices += '<option ' + (parseInt(opt.selectedKey, 10) === tmpKey ? 'selected="selected"' : '') + ' value="' + tmpKey + '" title="' + tmpLabel + '">' + tmpLabel + '</option>';
            });
        }
        strInputChoices = strInputChoices.replace(/\|$/, '');
        this.find('.ms-formbody input:first').attr('choices', strInputChoices);
        // changed selector not to select the greater than 25 elements sharepoint dropdown with id "_Select"
        this.find('.ms-formbody select:first:not(#_Select)').html(strSelectChoices);
    };

    /**
     * this function is applying a loader image to the dom
     *
     * @method
     * @param
     */

    $.fn.SPUseLoader = function (options) {
        var opt = options;
        if (!$('.speLoader')[0]) {
            // $('#aspnetForm').hide();
            $('body').prepend('<table class="speLoader" width="100%" border="0" align="center" style="width: 100%; height: 100%; position: absolute; border: none; background: #fff; background: rgba(255, 255, 255, 0.77); z-index: 9999;"><tr><td align="center"><img src="/_layouts/images/gears_an.gif"/></td></tr></table>');
        }
        if (opt) {
            $('.speLoader').show();
            // $('#aspnetForm').hide();
        } else {
            // $('#aspnetForm').show();
            $('.speLoader').hide();
        }
    };

    /**
     * this function is replacing the options in simple dropdowns
     *
     * @method
     * @param
     */

    $.fn.SPSetFieldChoices = function (options) {
        // $('').SPSetFieldChoices({dropdownItems: ["das","ist","ein","test"]});
        var opt = $.extend({
            "dropdownItems": false,
            "selectedItem": false
        }, options);
        var returnArr = opt.dropdownItems,
            thisIsSelected = opt.selectedItem;
        // find dropdown in dom
        var selectElement = $(this).find('.ms-formbody select.ms-RadioText');
        if ($(this).hasClass('SPFieldChoice') && opt.dropdownItems !== false) {
            // for option field
            var optionString = '';
            $.each(opt.dropdownItems, function (index, value) {
                optionString += '<option ' + (opt.selectedItem === value ? 'selected="selected"' : '') + ' value="' + value + '">' + value + '</option>';
            });
            selectElement.html(optionString);
        } else {
            returnArr = [];
            selectElement.find('option').each(function () {
                returnArr.push($(this).val());
                if ($(this).is(':selected')) {
                    thisIsSelected = $(this).val();
                }
            });
        }
        return {
            "dropdownItems": returnArr,
            "selectedItem": thisIsSelected
        };
    };

    /**
     * this function hepls you accessing the parameter values of an (current) url
     *
     * @method
     * @param
     */

    $.fn.SPEnhancements.queryParams = function (options) {
        var opt = options ? options : window.location.search;
        opt = opt.split('+').join(' ');
        var params = {},
            re = /[?&]?([^=]+)=([^&]*)/g,
            tokens;
        while ((tokens = re.exec(opt))) {
            try {
                params[decodeURIComponent(unescape(tokens[1]))] = decodeURIComponent(unescape(tokens[2]));
            } catch (e) {
                params[tokens[1]] = tokens[2];
            }
        }
        return params;
    };

    /**
     * adding labels / display text, according to the state of the boolean checkbox
     *
     * @method
     * @param
     */

    $.fn.SPAddBooleanLabel = function (options) {
        var opt = $.extend({
            "removeLabelAndTurnoff": false,
            "booleanLabels": {
                "yesLabel": "Yes",
                "noLabel": "No"
            }
        }, options);
        // find togglebox in dom
        var toggleElement = $(this).find('.ms-formbody input[type="checkbox"]:first');
        if (Boolean($(this).hasClass('SPFieldBoolean') && !opt.removeLabelAndTurnoff && options !== false) || options === true || typeof options === "undefined") {
            // for checkbox (yes/no) do this
            toggleElement.on('change speCheck', function () {
                $(this).next('.SPFieldBooleanDisplayText').remove();
                if ($(this).is(':checked')) {
                    $(this).after('<label for="' + $(this).attr('id') + '" class="SPFieldBooleanDisplayText">' + opt.booleanLabels.yesLabel + '</label>');
                } else {
                    $(this).after('<label for="' + $(this).attr('id') + '" class="SPFieldBooleanDisplayText">' + opt.booleanLabels.noLabel + '</label>');
                }
            });
            toggleElement.trigger('speCheck');
        } else {
            toggleElement.off('change speCheck').next('.SPFieldBooleanDisplayText').remove();
        }
        // update locked field state
        if ($(this).hasClass('SPFieldLocked')) {
            $(this).SPLockField();
        }
        // return selectors to keep jquery chaining
        return this;
    };
})(jQuery);