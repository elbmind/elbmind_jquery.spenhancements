SPEnhancements Beta
===================
### Enhancing SharePoint (2010 / 2013 / 2016) Forms using jQuery

Enhance your SharePoint Forms using this jQuery library.
This library is not perfect and far away from rock solid, but never the less we like to share it with the world. Feel free to give us a feedback.

Please **use only the latest provided tags for "production use"** you will find in the [download area](https://bitbucket.org/elbmind/elbmind_jquery.spenhancements/downloads) in the "Tags-Tab".

*Licensed under the [MIT license](https://opensource.org/licenses/MIT)*

#### Some features and functions
* easy access to SharePoint form fields via css class, depending on the SharePoint internal field name descriptions
```javascript
$().SPSetFormFieldSelectors({
    relatedData: false,
    returnInfo: false,
    removeSelectors: false
});
```

* get or set the selected Lookup-ID form Lookup-Columns
```javascript
$('.speForm .YourInternalFieldName').SPLookupIdVal(42);
```

* get or set the field content for different (not all) SharePoint field types; SPFieldText, SPFieldUser, SPFieldBoolean, SPFieldDateTime
```javascript
$('.speForm .YourInternalFieldName').SPFieldVal('your value');
```

* create a dropdown from a choice list based on text or number fields (SPFieldText, SPFieldNumber)
```javascript
$('.speForm .YourInternalFieldName').SPAddCustomDropdown({
    fieldTitle: 'your form row field label',
    fieldSelectorLabelOutput: false,
    dropdownCoices: 'this, is, my, dropdown, list',
    positionOfDropdown: $('.speForm .SPFieldRow'),
    positionIsAfter: false
});
```

* "lock" from fields to prevent change
```javascript
$('.speForm .YourInternalFieldName').SPLockField({
    showHintText: true,
    unlockField: false,
    useLookupUrl: false
});
```

* alter lookup choices from an 'SPFieldLookup' type field
```javascript
$('.speForm .YourInternalFieldName').SPUpdateLookupChoices({
    dropdownCoices: {[
        key: 1,
        label: 'must be an option from original dropdown choice']
    }
});
```

* alter dropdown choices from an 'SPFieldChoice' field type
```javascript
$('.speForm .YourInternalFieldName').SPSetFieldChoices({
    dropdownItems: ["this","is","a","test"],
    selectedItem: false || 'test'
});
```

* add an fullscreen loader to your form, via adding standard gears_an.gif image reference to the dom and show / hide it on demand
```javascript
$('.speForm').SPUseLoader(true || false);
```

* adding labels / display text, according to the state of the 'SPFieldBoolean' field type
```javascript
$('.speForm').SPAddBooleanLabel({
    removeLabelAndTurnoff: false,
    booleanLabels: {
        yesLabel: 'Check',
        noLabel: 'Nope'
    }
});
```



* show current Version of the library
```javascript
$().SPEnhancements.Version;
```

* get an array with all SharePoint form fields which are found
```javascript
$().SPEnhancements.selectorOutput;
```

* use the current type of form (newform, editform, dispform) for your adjustments
```javascript
$().SPEnhancements.fileName;
```

* get the current SharePoint 'ClientSchemaVersion' *currently only SharePoint 2010 supported*
```javascript
$().SPEnhancements.sharepointVersion;
```

* access the parameter values of an (current) url
```javascript
$().SPEnhancements.queryParams(null || 'www.yoururl.com?some=params&to=explore');
```

_ _ _ _ _
*Inspired by the awesome jQuery library [SPServices from Marc D. Anderson](http://spservices.codeplex.com/)*

*David Kahnt | elbmind GmbH | 2018*
